export interface Student {
  id: number;
  nume: string;
  prenume: string;
  nota: number;
}
