import { Component, OnInit } from '@angular/core';
import { Student } from '../students';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})

export class StudentComponent implements OnInit {

  student: Student = {
    id: 1,
    nume: "Ventuneac",
    prenume: "Andrei",
    nota: 9.5,
  };

  constructor() { }

  ngOnInit() {
  }


}
